//
//  CinemaDetailsModel.swift
//  FasterBook
//
//  Created by Reuben Tan on 7/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation

class CinemaDetailsTableModel {
    var date: String
    var cinemaDetailsSection = [CinemaDetailsSectionModel]()
    init(date: String) {
        self.date = date
    }
}

class CinemaDetailsSectionModel {
    var earliestTimeSlot: Date?
    var movie: Movie2
    var timing: [Timing2]
    init(movie: Movie2) {
        self.movie = movie
        self.timing = [Timing2]()
    }
}
