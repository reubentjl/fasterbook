//
//  ResponseData.swift
//  FasterBook
//
//  Created by Reuben Tan on 17/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation

// Start CinemeaDetailsSection
struct CinemaDetailsMovies: Codable {
    let statusResponse: String
    let data: [CinemaDetailsMovie]
}

struct CinemaDetailsMovie: Codable {
    let id, runtime: Int
    let is3D: Bool
    let title, synoposis, rating, imgPosterUrl, imgBannerUrl, trailerUrl, genre: String
    let timingList: [CinemaDetailsTiming]
}

struct CinemaDetailsTiming: Codable {
    let idtiming: Int
    let time: String
    let date: String
    let link: String
}
// End CinemeaDetailsSection

struct ResponseData: Codable {
    let statusResponse: String
    let data: [Movie]
}

struct Movie: Codable {
    let id, runtime: Int
    let title,synoposis,rating, genre: String
    let imgPosterUrl, imgBannerUrl, trailerUrl: String
    let is3D: Bool
    
    enum CodingKeys: String, CodingKey {
        case id,title,synoposis, rating, runtime, genre, is3D
        case imgPosterUrl = "imgPosterUrl"
        case imgBannerUrl = "imgBannerUrl"
        case trailerUrl = "trailerUrl"
    }
}

enum featuredSectionIds: Int {
    case cover = 0,
    synopsis, timing
}

struct Showtime: Codable {
    let statusResponse: String
    var data: [Datum]
}

struct Datum: Codable {
    let date: String
    var cinemas: [Cinema]
}

struct Cinemas: Codable {
    let statusResponse: String
    let data: [Cinema]
}

struct Cinema: Codable {
    let idcinema: Int
    let cinemaName: String
    let neighbourhood: String
    let address: String
    let postalCode: String
    let customType: String
    let cinemaCompany: CinemaCompany
    let timings: [Timing]
}

struct  Timing: Codable {
    let idtiming: Int
    let time: String
    let link: String
}

enum CinemaCompany: String, Codable {
    case cathay = "Cathay"
    case goldenVillage = "Golden Village"
}

struct Cover {
    let imgBannerUrl, rating: String
    let runtime: Int
    let noOfItems = 1
}

struct Synoposis {
    let synoposis: String
    let noOfItems = 1
}

struct Timings {
    let timings: [Timing]
    let noOfItems: Int
}

struct FeaturedSections {
    let movie: Movie2
    let coverSection: Cover
    let synoposisSection: Synoposis
    var timingsSection: Timings?
    let noOfSections = 3
}

// Struct for Search
struct Search: Codable {
    let statusResponse: String
    let data: SearchData
}

struct SearchData: Codable {
    let movie: [MovieSearch]
    let cinema: [CinemaSearch]
}

struct MovieSearch: Codable {
    let id, runtime: Int
    let title,rating: String
    let is3D: Bool
    
    enum CodingKeys: String, CodingKey {
        case id,title, rating, runtime, is3D
    }
}

struct CinemaSearch: Codable {
    let idcinema: Int
    let cinemaName: String
    let customType: String
    let cinemaCompany: CinemaCompany
}

// Struct for Search Exist
struct SearchExist: Codable {
    let statusResponse: String
    let data: SearchExistData
}

struct SearchExistData: Codable {
    let exist: Bool
}

// Struct for Search Details
struct SearchDetails: Codable {
    let statusResponse: String
    let data: SearchDetailsData
}

struct SearchDetailsData: Codable {
    let exist: Bool
    let movie: SearchDetailsMovie
    let cinema: SearchDetailsCinema
}

struct SearchDetailsMovie: Codable {
    let id, runtime: Int
    let title, synoposis, rating, imgPosterUrl, imgBannerUrl, trailerUrl, genre: String
    let is3D: Bool
}

struct SearchDetailsCinema: Codable {
    let idcinema: Int
    let cinemaName, address, postalCode, customType: String
    let cinemaCompany: CinemaCompany
    let timings: [Timing]
}










