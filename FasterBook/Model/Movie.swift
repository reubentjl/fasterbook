//
//  Movie.swift
//  FasterBook
//
//  Created by Reuben Tan on 12/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation

struct CinemasMovies2: Decodable {
    let statusResponse: String
    let data: [Cinema2]
}

struct MoviesResponse: Decodable {
    let statusResponse: String
    let data: [Movie2]?
}

struct ShowTimingResponse: Decodable {
    let statusResponse: String
    let data: [Showtime2]
}

struct SearchResponse: Decodable {
    let statusResponse: String
    let data: SearchData2
}

struct SearchDetailsResponse: Decodable {
    let statusResponse: String
    let data: SearchDetailsData2
}

struct SearchData2: Decodable {
    let exist: Bool?
    let movieList: [Movie2]
    let cinemaList: [Cinema2]
    enum CodingKeys: String, CodingKey {
        case exist
        case movieList = "movie"
        case cinemaList = "cinema"
    }
}

struct SearchDetailsData2: Decodable {
    let exist: Bool
    let movie: Movie2
    let cinema: Cinema2
}

struct Showtime2: Decodable {
    let date: String
    let cinemaList: [Cinema2]
    enum CodingKeys: String, CodingKey {
        case date
        case cinemaList = "cinemas"
    }
}

struct Movie2: Decodable {
    let id: Int
    let title, synoposis, rating, genre: String?
    let runtime: Int?
    let imgPosterUrl: String?
    let imgBannerUrl, trailerUrl: String?
    let is3D: Bool
    let timingList: [Timing2]?
    
    init(id: Int, title: String? = nil, synoposis: String? = nil, rating: String? = nil, genre: String? = nil, runtime: Int? = nil, imgPosterUrl: String? = nil, imgBannerUrl: String? = nil, trailerUrl: String? = nil, is3D: Bool,
        timingList:[Timing2]? = nil) {
        self.id = id
        self.title = title
        self.synoposis = synoposis
        self.rating = rating
        self.genre = genre
        self.runtime = runtime
        self.imgPosterUrl = imgPosterUrl
        self.imgBannerUrl = imgBannerUrl
        self.trailerUrl = trailerUrl
        self.is3D = is3D
        self.timingList = timingList
    }
}

struct Timing2: Decodable {
    let id: Int
    let date, time, link: String?
    
    enum CodingKeys: String, CodingKey {
        case date, time, link
        case id = "idtiming"
    }
}

struct Cinema2: Decodable {
    let id: Int
    let name, company, neighbourhood, address, postalCode, customType: String?
    let timingList: [Timing2]?
    let showingMovieList: [Movie2]?
    
    enum CodingKeys: String, CodingKey {
        case neighbourhood, address, postalCode, customType
        case id = "idcinema"
        case name = "cinemaName"
        case company = "cinemaCompany"
        case timingList = "timings"
        case showingMovieList = "showingMovie"
    }
    
    init(id: Int, name: String? = nil, company: String? = nil, neighbourhood: String? = nil, address: String? = nil, postalCode: String? = nil, customType: String? = nil, timingList: [Timing2]? = nil, showingMovieList: [Movie2]? = nil) {
        self.id = id
        self.name = name
        self.company = company
        self.neighbourhood = neighbourhood
        self.address = address
        self.postalCode = postalCode
        self.customType = customType
        self.timingList = timingList
        self.showingMovieList = showingMovieList
    }
}
