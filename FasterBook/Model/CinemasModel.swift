//
//  CinemasModel.swift
//  FasterBook
//
//  Created by Reuben Tan on 7/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation

struct CinemasMovies: Codable {
    let statusResponse: String
    let data: [CinemasMoviesData]
}

struct CinemasMoviesData: Codable {
    let idmovie, idcinema: Int
    let is3D: Bool
    let cinemaName, neighbourhood, posterPath, customType: String
    let company: CinemaCompany
}

struct CinemasMoviesSection {
    let section: String
    var rows: [CinemasMoviesRow]
    init(section: String) {
        self.section = section
        rows = [CinemasMoviesRow]()
    }
}

struct CinemasMoviesRow {
    let idCinema: Int
    let cinemaName, neighbourhood, customType: String
    var collections:[CinemaMoviesCollection]
    init(id: Int, name: String, neighbourhood: String, customType: String) {
        self.idCinema = id
        self.cinemaName = name
        self.neighbourhood = neighbourhood
        self.customType = customType
        collections = [CinemaMoviesCollection]()
    }
}

struct CinemaMoviesCollection {
    let idmovie: Int
    let posterPath: String
    let is3D: Bool
}


