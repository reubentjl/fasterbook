//
//  TimingsDataModel.swift
//  FasterBook
//
//  Created by Reuben Tan on 1/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation
class TimingTableModel: CustomStringConvertible {
    var date: String
    var sectionModels = [TimingTableSectionModel]()
    init(date: String) {
        self.date = date
    }
    
    var description: String {
        var result = date
        for section in sectionModels {
            result += "\(section)\n"
        }
        return result
    }
}

class TimingTableSectionModel: CustomStringConvertible {
    var sectionTitle: String
    var date: Date?
    var collectionModels = [TimingTableCollectionModel]()
    init(sectionTitle: String) {
        self.sectionTitle = sectionTitle
    }
    
    var description: String {
        var result = sectionTitle
        for collection in collectionModels {
            result += "\(collection)\n"
        }
        return result
    }
}

class TimingTableCollectionModel: CustomStringConvertible {
    var modelTitle: String
    var timings = [Timing2]()
    init(modelTitle: String, timings: [Timing2]) {
        self.modelTitle = modelTitle
        self.timings = timings
    }
    init(modelTitle: String) {
        self.modelTitle = modelTitle
    }
    var description: String {
        return "\(modelTitle) \(timings)"
    }
}

class CinemaTableModel: CustomStringConvertible {
    var company: String
    var cinemas = [Cinema2]()
    init(company: String) {
        self.company = company
    }
    var description: String {
        return company + "\(cinemas)"
    }
}







