//
//  CinemaDetailsMovieCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 7/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class CinemaDetailsMovieCell: UITableViewCell {

    @IBOutlet weak var is3DLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var synopsisLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
