//
//  TrailerMovieDetailsCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import WebKit

class TrailerMovieDetailsCell: UITableViewCell, WKNavigationDelegate {
    @IBOutlet weak var trailerWebView: WKWebView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        trailerWebView.navigationDelegate = self
        trailerWebView.scrollView.isScrollEnabled = false
        // Initialization code 
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

}
