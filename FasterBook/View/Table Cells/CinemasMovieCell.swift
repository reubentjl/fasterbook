//
//  CinemasMovieCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 7/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

protocol CinemasMovieCellDelegate: class {
    func collectionViewImgSelected(collectioncell: CinemasImageCell?, didTappedInTableView TableCell: CinemasMovieCell)
}

class CinemasMovieCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var imagesCollection: UICollectionView!
    
    var movies: [CinemaMoviesCollection]?
    
    weak var cellDelegate: CinemasMovieCellDelegate?
    var indexPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imagesCollection.delegate = self
        self.imagesCollection.dataSource = self
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if movies != nil {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if movies != nil {
            return movies!.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if movies != nil {
            if let cell = imagesCollection.dequeueReusableCell(withReuseIdentifier: "CinemasImageCell", for: indexPath) as?  CinemasImageCell {
                cell.movieId = movies![indexPath.row].idmovie
                cell.movieImage.contentMode = .scaleAspectFill
                if movies![indexPath.row].is3D {
                    cell.is3DLabel.text = "3D"
                    cell.is3DLabel.textColor = .white
                    cell.is3DLabel.layer.shadowColor = UIColor.black.cgColor
                    cell.is3DLabel.layer.shadowRadius = 3.0
                    cell.is3DLabel.layer.shadowOpacity = 1.0
                    cell.is3DLabel.shadowOffset = CGSize(width: 3, height: 3)
                    cell.is3DLabel.layer.masksToBounds = false
                } else {
                    cell.is3DLabel.text = ""
                }
                if movies![indexPath.row].posterPath.starts(with: "http") {
                    cell.movieImage.downloadedFrom(link: movies![indexPath.row].posterPath)
                } else {
                    cell.movieImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w185" + movies![indexPath.row].posterPath)
                }
                return cell
            }
            
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CinemasImageCell
        self.cellDelegate?.collectionViewImgSelected(collectioncell: cell, didTappedInTableView: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
