//
//  CinemasCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 6/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class CinemasCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
