//
//  CinemaCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

protocol CustomCollectionCellDelegate: class {
    func collectionView(collectioncell: TimeCell?, didTappedInTableView TableCell: CinemaCell)
}

class CinemaCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    weak var cellDelegate: CustomCollectionCellDelegate?
    
    @IBOutlet weak var cinemaLabel: UILabel!
    @IBOutlet weak var timeCollection: UICollectionView!
    
    var timings: [Timing2]?
    
//    func setCollectionViewDataSourceDelegate <D: UICollectionViewDelegate & UICollectionViewDataSource> (_ dataSourceDelegate: D, forRow section: Int) {
//        timeCollection.delegate = dataSourceDelegate
//        timeCollection.dataSource = dataSourceDelegate
//        timeCollection.tag = section
//        timeCollection.reloadData()
//    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.timings = nil
        self.cinemaLabel.text = ""
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timeCollection.delegate = self
        self.timeCollection.dataSource = self
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if timings != nil {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if timings != nil {
            return timings!.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? TimeCell
        self.cellDelegate?.collectionView(collectioncell: cell, didTappedInTableView: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if timings != nil {
            if let cell = timeCollection.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as? TimeCell {
                if timings![indexPath.row].link! == "" {
                    cell.backgroundColor = UIColor(red:0.96, green:0.26, blue:0.21, alpha:1.0)
                } else {
                    cell.backgroundColor = UIColor(red:0.50, green:0.78, blue:0.49, alpha:1.0)
                }
                cell.timeLabel.text = timings![indexPath.row].time!
                cell.link = timings![indexPath.row].link!
                return cell
            }
        }
        return UICollectionViewCell()
    }
}
