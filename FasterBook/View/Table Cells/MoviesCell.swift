//
//  MoviesCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

protocol ShowTimingButtonDelegate: class {
    func showTimingButtonTapped(at index: IndexPath)
}

class MoviesCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var runtimeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var is3DLabel: UILabel!
    
    var indexPath:IndexPath!
    weak var delegate: ShowTimingButtonDelegate?
    
    @IBAction func showTimingButtonAction(_ sender: UIButton) {
        print("button clicked")
        self.delegate?.showTimingButtonTapped(at: indexPath)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
