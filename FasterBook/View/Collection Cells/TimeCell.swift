//
//  TimeCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class TimeCell: UICollectionViewCell {
    @IBOutlet weak var timeLabel: UILabel!
    var link: String?
    
    override func prepareForReuse() {
        self.timeLabel.text = ""
        if link != nil {
            link = nil
        }
    }
}
 
