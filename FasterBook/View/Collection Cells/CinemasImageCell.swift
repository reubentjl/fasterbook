//
//  CinemasImageCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 7/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class CinemasImageCell: UICollectionViewCell {
    @IBOutlet weak var is3DLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    var movieId: Int!
}
