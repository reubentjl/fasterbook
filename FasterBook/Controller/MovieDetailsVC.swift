//
//  MovieDetailsVC.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MovieDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var adBanner: GADBannerView!
    @IBOutlet weak var movieDetailsTable: UITableView!
    var detailMovie: Movie2? {
        didSet {
            configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        movieDetailsTable.delegate = self
        movieDetailsTable.dataSource = self
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let movie = detailMovie {
            if section == MovieDetailsTableEnum.trailer.rawValue {
                return (movie.trailerUrl != "") ? 1 : 0
            } else if section == MovieDetailsTableEnum.synopsis.rawValue {
                return (movie.synoposis != "") ? 1 : 0
            } else if section == MovieDetailsTableEnum.genre.rawValue {
                return (movie.genre != "" && movie.genre != "null") ? 1 : 0
            } else if section == MovieDetailsTableEnum.runtime.rawValue {
                return (movie.runtime != 0) ? 1 : 0
            } else if section == MovieDetailsTableEnum.rating.rawValue {
                return (movie.rating != "" && movie.rating != "-") ? 1 : 0
            } else if section == MovieDetailsTableEnum.images.rawValue {
                return (movie.imgBannerUrl != "" || movie.imgPosterUrl != "") ? 1 : 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let movie = detailMovie {
            if section == MovieDetailsTableEnum.trailer.rawValue {
                return (movie.trailerUrl != "") ? "Trailer" : ""
            } else if section == MovieDetailsTableEnum.synopsis.rawValue {
                return (movie.synoposis != "") ? "Synopsis" : ""
            } else if section == MovieDetailsTableEnum.genre.rawValue {
                return (movie.genre != "" && movie.genre != "null") ? "Genre" : ""
            } else if section == MovieDetailsTableEnum.runtime.rawValue {
                return (movie.runtime != 0) ? "Runtime" : ""
            } else if section == MovieDetailsTableEnum.rating.rawValue {
                return (movie.rating != "" && movie.rating != "-") ? "Rating" : ""
            } else if section == MovieDetailsTableEnum.images.rawValue {
                return (movie.imgBannerUrl != "" || movie.imgPosterUrl != "") ? "Images" : ""
            }
        }
        return ""
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let detailMovie = detailMovie {
            let section = indexPath.section
            if section == MovieDetailsTableEnum.trailer.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "TrailerCell", for: indexPath) as! TrailerMovieDetailsCell
                let url = URL(string: "https://www.youtube.com/embed/" + detailMovie.trailerUrl!)
                let request = URLRequest(url: url!)
                cell.trailerWebView.load(request)
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == MovieDetailsTableEnum.synopsis.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = detailMovie.synoposis
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == MovieDetailsTableEnum.genre.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = detailMovie.genre
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == MovieDetailsTableEnum.runtime.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = "\(detailMovie.runtime!) mins"
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == MovieDetailsTableEnum.rating.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = detailMovie.rating
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == MovieDetailsTableEnum.images.rawValue {
                let cell = movieDetailsTable.dequeueReusableCell(withIdentifier: "ImagesCell", for: indexPath) as! ImagesMovieDetailsCell
                if detailMovie.imgBannerUrl != "" {
                    cell.bannerImage.contentMode = .scaleAspectFill
                    if detailMovie.imgBannerUrl!.starts(with: "http") {
                        cell.bannerImage.downloadedFrom(link: detailMovie.imgBannerUrl!)
                    } else {
                        cell.bannerImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w500/" + detailMovie.imgBannerUrl!)
                    }
                }
                if detailMovie.imgPosterUrl != "" {
                    cell.posterImage.contentMode = .scaleAspectFill
                    if detailMovie.imgPosterUrl!.starts(with: "http") {
                        cell.posterImage.downloadedFrom(link: detailMovie.imgPosterUrl!)
                    } else {
                        cell.posterImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w185/" + detailMovie.imgPosterUrl!)
                    }
                    
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        return UITableViewCell()
    }
     
    func configureView() {
        if detailMovie != nil {
            if detailMovie!.is3D {
                navigationItem.title = "(3D) " + detailMovie!.title!
            } else {
                navigationItem.title = detailMovie!.title
            }
            if let movieDetailsTable = movieDetailsTable {
                movieDetailsTable.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let timingVC = segue.destination as? TimingsVC {
            timingVC.movieId = (self.detailMovie?.id)!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
