//
//  BookTimingVC.swift
//  FasterBook
//
//  Created by Reuben Tan on 9/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import WebKit

class BookTimingVC: UIViewController, WKNavigationDelegate {
    var link: String?

    @IBOutlet weak var bookingWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookingWebView.navigationDelegate = self
        updateVC()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateVC() {
        if let link = self.link {
            let url = URL(string: link)
            let request = URLRequest(url: url!)
            self.navigationItem.title = "Loading Booking Site"
            bookingWebView.load(request)
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.navigationItem.title = "Book Tickets"
    }

}
