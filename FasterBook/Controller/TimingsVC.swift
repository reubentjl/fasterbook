//
//  TimingsVC.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class TimingsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var movieId: Int? {
        didSet {
            getData()
        }
    }
    var showtime: ShowTimingResponse?
    var timingModels = [TimingTableModel]()
    var availableDates : [String] = []
    var segmentIndex: Int?
    var timingModelIndex: Int?
    
    @IBOutlet weak var adBanner: GADBannerView!
    @IBOutlet weak var dateControl: UISegmentedControl!
    @IBOutlet weak var timeTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Timings"
        setupTable()
        getData()
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
    }
    
    @IBAction func dateSelected(_ sender: UISegmentedControl) {
        segmentIndex = sender.selectedSegmentIndex
        timingModelIndex = timingModels.index(where: {$0.date == availableDates[segmentIndex!]})
        print("Updated modelIndex: \(timingModelIndex!)")
        timeTable.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if timingModelIndex != nil {
            return timingModels[timingModelIndex!].sectionModels.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if timingModelIndex != nil {
            return timingModels[timingModelIndex!].sectionModels[section].sectionTitle
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if timingModelIndex != nil {
            return timingModels[timingModelIndex!].sectionModels[section].collectionModels.count
        }
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if timingModelIndex != nil {
            let cell = timeTable.dequeueReusableCell(withIdentifier: "CinemaCell", for: indexPath) as! CinemaCell
            //print("timingModelIndex: \(timingModelIndex!), section: \(indexPath.section), row: \(indexPath.row)")
            cell.timings = timingModels[timingModelIndex!].sectionModels[indexPath.section].collectionModels[indexPath.item].timings
            cell.cinemaLabel.text = timingModels[timingModelIndex!].sectionModels[indexPath.section].collectionModels[indexPath.item].modelTitle
            cell.cellDelegate = self
            cell.timeCollection.reloadData()
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
        return UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        guard let tableViewCell = cell as? CinemaCell else { return }
//        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
    @IBAction func sortBtn(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let a2zAction = UIAlertAction(title: "A-Z (Cinema)", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingModels = self.sortByCinema(showtime: self.showtime!)
            self.timeTable.reloadData()
        })
        let timingAction = UIAlertAction(title: "Timing", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingModels = self.sortByTiming(showtime: self.showtime!)
            self.timeTable.reloadData()
        })
        let neighbourhoodAction = UIAlertAction(title: "Neighbourhood", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingModels = self.sortByNeighbourhood(showtime: self.showtime!)
            self.timeTable.reloadData()
        })
//        let nearestFirstAction = UIAlertAction(title: "Nearest First", style: .default, handler: {(alert: UIAlertAction!) -> Void in
//
//        })
        alertController.addAction(a2zAction)
        alertController.addAction(timingAction)
        alertController.addAction(neighbourhoodAction)
        //alertController.addAction(nearestFirstAction)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setupTable() {
        timeTable.delegate = self
        timeTable.dataSource = self
    }
    
    func getData() {
        if let movieId = movieId {
            if (timeTable) != nil {
                let url = URL(string: "http://52.230.25.86:4567/showtiming/\(movieId)")
                //let url = URL(string: "http://192.168.1.91:4567/showtiming/\(movieId)")
                print("URL: \(url!)")
                URLSession.shared.dataTask(with: url!) { (data, response, error) in
                    if error == nil {
                        do {
                            self.showtime = try JSONDecoder().decode(ShowTimingResponse.self, from: data!)
                        } catch {
                            print("Parse Error: \(error)")
                            let alert = UIAlertController(title: "Error parsing Data", message: error as! String, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                            self.present(alert, animated: true)
                        }
                       DispatchQueue.main.sync {
                            self.timingModels = self.sortByCinema(showtime: self.showtime!)
                            var date = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd"
                            for i in 0...2 {
                                let result = formatter.string(from: date)
                                self.availableDates.append(result)
                                if !self.timingModels.contains(where: {$0.date == result})  {
                                    self.dateControl.setEnabled(false, forSegmentAt: i)
                                }
                                date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
                            }
                            date = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
                            formatter.dateFormat = "EEEE"
                            self.dateControl.setTitle(formatter.string(from: date), forSegmentAt: 2)
                            for i in 0..<self.availableDates.count {
                                if let index = self.timingModels.index(where: {$0.date == self.availableDates[i]}) {
                                    if self.timingModels[index].sectionModels.count != 0 {
                                        self.dateControl.selectedSegmentIndex = i
                                        self.timingModelIndex = index
                                        break
                                    }
                                }
                            }
                            if self.timingModelIndex == nil {
                                self.timingModelIndex = self.timingModels.index(where: {$0.date == self.availableDates[0]})
                            }
                            //self.timingModelIndex = self.timingModels.index(where: {$0.date == self.availableDates[0]})
                            self.timeTable.reloadData()
                            print("URL: load completed")
                        }
                    } else {
                        let alert = UIAlertController(title: "Error Loading Data", message: error.debugDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    }.resume()
            }
        }
    }
    
    func sortByCinema(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var timingTableModels = [TimingTableModel]()
        if showtime.data.count == 0 {
            return timingTableModels
        }
        for day in showtime.data {
            for cinema in day.cinemaList {
                let company = cinema.company!
                var name = cinema.name!
                if cinema.customType != "Normal" {
                    name = name + " (" + cinema.customType! + ")"
                }
                for timing in cinema.timingList! {
                    let timingId = timing.id
                    let time = timing.time!
                    let newTiming = Timing2(id: timingId, date: timing.date, time: time, link: timing.link)
                    
                    if let timingModelIndex = timingTableModels.index(where: {$0.date == day.date}) {
                        // day exists
                        if let sectionIndex = timingTableModels[timingModelIndex].sectionModels.index(where: {$0.sectionTitle == company}) {
                           // section exists
                            if let collectionIndex = timingTableModels[timingModelIndex].sectionModels[sectionIndex].collectionModels
                                .index(where: {$0.modelTitle == name}) {
                                // collection exists
                                timingTableModels[timingModelIndex]
                                    .sectionModels[sectionIndex]
                                    .collectionModels[collectionIndex]
                                    .timings.append(newTiming)
                            } else {
                                // collection doesn't exist
                                let collectionModel = TimingTableCollectionModel(modelTitle: name)
                                collectionModel.timings.append(newTiming)
                                timingTableModels[timingModelIndex]
                                    .sectionModels[sectionIndex]
                                    .collectionModels.append(collectionModel)
                            }
                        } else {
                            // section doesn't exists
                            let sectionModel = TimingTableSectionModel(sectionTitle: company)
                            let collectionModel = TimingTableCollectionModel(modelTitle: name)
                            collectionModel.timings.append(newTiming)
                            sectionModel.collectionModels.append(collectionModel)
                            timingTableModels[timingModelIndex].sectionModels.append(sectionModel)
                        }
                    } else {
                        // day doesn't exists
                        let dayModel = TimingTableModel(date: day.date)
                        let sectionModel = TimingTableSectionModel(sectionTitle: company)
                        let collectionModel = TimingTableCollectionModel(modelTitle: name)
                        collectionModel.timings.append(newTiming)
                        sectionModel.collectionModels.append(collectionModel)
                        dayModel.sectionModels.append(sectionModel)
                        timingTableModels.append(dayModel)
                    }
                }
            }
        }
        
        for dayIndex in 0..<timingTableModels.count {
            timingTableModels[dayIndex].sectionModels.sort(by: {$0.sectionTitle < $1.sectionTitle})
            for sectionIndex in 0..<timingTableModels[dayIndex].sectionModels.count {
                timingTableModels[dayIndex].sectionModels[sectionIndex].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
        }

        return timingTableModels
    }
    
    func sortByNeighbourhood(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var timingTableModels = [TimingTableModel]()
        for day in showtime.data {
            var dayModel = TimingTableModel(date: day.date)
            var sectionModels = [TimingTableSectionModel]()
            for cinema in day.cinemaList {
                var collectionModel = TimingTableCollectionModel(modelTitle: cinema.name!, timings: cinema.timingList!)
                let sectionIndex = sectionModels.index(where: {$0.sectionTitle == cinema.neighbourhood})
                if sectionIndex != nil {
                    //  Section exists
                    sectionModels[sectionIndex!].collectionModels.append(collectionModel)
                } else {
                    //  Section doesn't exist
                    var newSectionModel = TimingTableSectionModel(sectionTitle: cinema.neighbourhood!)
                    newSectionModel.collectionModels.append(collectionModel)
                    sectionModels.append(newSectionModel)
                }
            }
            dayModel.sectionModels = sectionModels
            timingTableModels.append(dayModel)
        }
        for dayIndex in 0..<timingTableModels.count {
            timingTableModels[dayIndex].sectionModels.sort(by: {$0.sectionTitle < $1.sectionTitle})
            for sectionIndex in 0..<timingTableModels[dayIndex].sectionModels.count {
                timingTableModels[dayIndex].sectionModels[sectionIndex].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
        }
        return timingTableModels
    }
    
    func sortByTiming(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var sortedTimingTableModels = [TimingTableModel]()
        
        for i in 0..<self.showtime!.data.count {
            var timingTableModel = TimingTableModel(date: (self.showtime?.data[i].date)!)
            var sectionModels = [TimingTableSectionModel]()
            for j in 0..<self.showtime!.data[i].cinemaList.count {
                let cinemaName = self.showtime!.data[i].cinemaList[j].name!
                for time in self.showtime!.data[i].cinemaList[j].timingList! {
                    let hourAndAmOrPm : String
                    var date: Date
                    if time.time!.hasSuffix("AM") || time.time!.hasSuffix("PM") {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "h:mma"
                        date = dateFormatter.date(from: time.time!)!
                        dateFormatter.dateFormat = "h a"
                        hourAndAmOrPm = dateFormatter.string(from: date)
                        date = dateFormatter.date(from: hourAndAmOrPm)!
                    } else {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm"
                        date = dateFormatter.date(from: time.time!)!
                        dateFormatter.dateFormat = "h a"
                        hourAndAmOrPm = dateFormatter.string(from: date)
                        date = dateFormatter.date(from: hourAndAmOrPm)!
                    }
                    if let existsSectionModel = sectionModels.first(where: {$0.sectionTitle == hourAndAmOrPm}) {
                        if let cinemaCollectionModelExists = existsSectionModel.collectionModels.first(where: {$0.modelTitle == cinemaName}) {
                            cinemaCollectionModelExists.timings.append(time)
                        } else {
                            let timings = [time]
                            let collectionModel = TimingTableCollectionModel(modelTitle: cinemaName, timings: timings)
                            existsSectionModel.collectionModels.append(collectionModel)
                        }
                    } else {
                        var newSectionModel = TimingTableSectionModel(sectionTitle: hourAndAmOrPm)
                        newSectionModel.date = date
                        let timings = [time]
                        let collectionModel = TimingTableCollectionModel(modelTitle: cinemaName, timings: timings)
                        newSectionModel.collectionModels.append(collectionModel)
                        sectionModels.append(newSectionModel)
                    }
                }
            }
            sectionModels.sort(by: {$0.date!.compare($1.date!) == .orderedAscending})
            for j in 0..<sectionModels.count {
                sectionModels[j].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
            timingTableModel.sectionModels = sectionModels
            sortedTimingTableModels.append(timingTableModel)
        }
        
        return sortedTimingTableModels
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TimingsVC: CustomCollectionCellDelegate {
    func collectionView(collectioncell: TimeCell?, didTappedInTableView TableCell: CinemaCell) {
        if let cell = collectioncell {
            if cell.link != "" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "BookTimingVC") as? BookTimingVC
                controller?.link = cell.link
                self.navigationController?.pushViewController(controller!, animated: true)
            } else {
                let alert = UIAlertController(title: "Sold out!", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
}

//extension TimingsVC: UICollectionViewDelegate, UICollectionViewDataSource {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if timingModelIndex != nil {
//            return timingModels[timingModelIndex!]
//                .sectionModels[collectionView.tag]
//                .collectionModels[TableRowNo].timings.count
//        }
//        return 0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeCell", for: indexPath) as! TimeCell
//        cell.timeLabel.text = timingModels[timingModelIndex!].sectionModels[collectionView.tag].collectionModels[indexPath.section].timings[indexPath.item].time
//        return cell
//    }
//}
