//
//  CinemaDetailsVC.swift
//  FasterBook
//
//  Created by Reuben Tan on 6/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class CinemaDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var daySemgmentControl: UISegmentedControl!
    @IBOutlet weak var cinemaTable: UITableView!
    var idcinema: Int?
    var cinemaName: String?
    var cinemaDetailsMovie = [Movie2]()
    var segmentIndex: Int?
    var tableModelIndex: Int?
    
    var tableModels = [CinemaDetailsTableModel]()
    var availableDates : [String] = []
    
    var scrollToMovieId: Int?
    var selectedDate: String?
    
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        fetchData()
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableModelIndex != nil {
            print("No of sections: \(tableModels[tableModelIndex!].cinemaDetailsSection.count)")
            return tableModels[tableModelIndex!].cinemaDetailsSection.count
        }
        print("No of sections: 0")
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableModelIndex != nil {
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableModelIndex != nil {
            return tableModels[tableModelIndex!].cinemaDetailsSection[section].movie.title! + " (" +
                tableModels[tableModelIndex!].cinemaDetailsSection[section].movie.rating! + ")"
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableModelIndex != nil {
            if indexPath.row == 0 {
                let cell = cinemaTable.dequeueReusableCell(withIdentifier: "CinemaDetailsMovieCell", for: indexPath) as! CinemaDetailsMovieCell
                let movie = tableModels[tableModelIndex!].cinemaDetailsSection[indexPath.section].movie
                cell.synopsisLabel.text = movie.synoposis
                cell.genreLabel.text = movie.genre
                print("\(movie)")
                if movie.is3D {
                    cell.is3DLabel.text = "3D"
                    cell.is3DLabel.textColor = .white
                    cell.is3DLabel.layer.shadowColor = UIColor.black.cgColor
                    cell.is3DLabel.layer.shadowRadius = 3.0
                    cell.is3DLabel.layer.shadowOpacity = 1.0
                    cell.is3DLabel.shadowOffset = CGSize(width: 3, height: 3)
                    cell.is3DLabel.layer.masksToBounds = false
                } else {
                    cell.is3DLabel.text = ""
                }
                cell.posterImage.contentMode = .scaleAspectFill
                if movie.imgPosterUrl!.starts(with: "http") {
                    cell.posterImage.downloadedFrom(link: movie.imgPosterUrl!)
                } else {
                    cell.posterImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w185/" + movie.imgPosterUrl!)
                }
                
                return cell
            }
            if indexPath.row == 1 {
                let cell = cinemaTable.dequeueReusableCell(withIdentifier: "CinemaCell", for: indexPath) as! CinemaCell
                let timing = tableModels[tableModelIndex!].cinemaDetailsSection[indexPath.section].timing
                cell.cinemaLabel.text = "Click to book"
                cell.timings = tableModels[tableModelIndex!].cinemaDetailsSection[indexPath.section].timing
                cell.timeCollection.reloadData()
                cell.cellDelegate = self
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 102
        }
        return UITableViewAutomaticDimension
    }
    
    @IBAction func daySelected(_ sender: UISegmentedControl) {
        segmentIndex = sender.selectedSegmentIndex
        tableModelIndex = tableModels.index(where: {$0.date == availableDates[segmentIndex!]})
        cinemaTable.reloadData()
    }
    
    @IBAction func sortButtonAction(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let a2zAction = UIAlertAction(title: "A-Z (Movie)", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.sortByTitle()
            self.cinemaTable.reloadData()
        })
        let earliestTimeAction = UIAlertAction(title: "Movie Time", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.sortByEarliestTime()
            self.cinemaTable.reloadData()
        })
        alertController.addAction(a2zAction)
        alertController.addAction(earliestTimeAction)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func setupTableView() {
        if cinemaName != nil {
            navigationItem.title = cinemaName!
        }
        cinemaTable.delegate = self
        cinemaTable.dataSource = self
    }
    
    func fetchData() {
        let url = URL(string: "http://52.230.25.86:4567/cinemaDetails/\(idcinema!)")
        //let url = URL(string: "http://192.168.1.91:4567/cinemaDetails/\(idcinema!)")
        print("URL: \(url!)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    let response = try JSONDecoder().decode(MoviesResponse.self, from: data!)
                    if response.data != nil {
                        self.cinemaDetailsMovie = response.data!
                    }
                } catch {
                    print("Parse Error: \(error)")
                    let alert = UIAlertController(title: "Error parsing Data", message: error as! String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                DispatchQueue.main.async {
                    self.tableModels = self.parseData(movies: self.cinemaDetailsMovie)
                    var date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    for i in 0...2 {
                        let result = formatter.string(from: date)
                        self.availableDates.append(result)
                        if !self.tableModels.contains(where: {$0.date == result})  {
                            self.daySemgmentControl.setEnabled(false, forSegmentAt: i)
                        }
                        date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
                    }
                    date = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
                    formatter.dateFormat = "EEEE"
                    self.daySemgmentControl.setTitle(formatter.string(from: date), forSegmentAt: 2)
                    self.tableModelIndex = self.tableModels.index(where: {$0.date == self.availableDates[0]})
                    self.cinemaTable.reloadData()
                    
                    if let selectedDate = self.selectedDate {
                        self.segmentIndex = self.availableDates.index(of: selectedDate)
                        if self.segmentIndex != nil {
                            self.tableModelIndex = self.tableModels.index(where: {$0.date == self.availableDates[self.segmentIndex!]})
                            self.daySemgmentControl.selectedSegmentIndex = self.segmentIndex!
                            self.cinemaTable.reloadData()
                        }
                    }
                    if let scrollToMovieId = self.scrollToMovieId {
                        if let sectionId = self.tableModels[self.tableModelIndex!].cinemaDetailsSection.index(where: {$0.movie.id == self.scrollToMovieId}) {
                            self.cinemaTable.scrollToRow(at: IndexPath(row: 0, section: sectionId), at: .top, animated: true)
                        }
                    }
                    for i in 0..<self.availableDates.count {
                        if let index = self.tableModels.index(where: {$0.date == self.availableDates[i]}) {
                            if self.tableModels[index].cinemaDetailsSection.count != 0 {
                                self.daySemgmentControl.selectedSegmentIndex = i
                                self.tableModelIndex = index
                                break
                            }
                        }
                    }
                    if self.tableModelIndex == nil {
                        self.tableModelIndex = self.tableModels.index(where: {$0.date == self.availableDates[0]})
                    }
                    if self.tableModels.count == 0 {
                        let alert = UIAlertController(title: "No movies available", message: "Please select another cinema", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                        self.present(alert, animated: true)
                    }
                    self.cinemaTable.reloadData()
                    print("URL: load completed \(self.cinemaTable.numberOfSections)")
                }
            } else {
                let alert = UIAlertController(title: "Error Loading Data", message: error.debugDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            }.resume()
    }
    
    func parseData(movies: [Movie2]) -> [CinemaDetailsTableModel] {
        var tableModels = [CinemaDetailsTableModel]()
        
        for movie in movies {
            for timing in movie.timingList! {
                let newTiming = Timing2(id: timing.id, date: timing.date, time: timing.time, link: timing.link)
                let dayIndex = tableModels.index(where: {$0.date == timing.date})
                if dayIndex != nil {
                    // day model exists
                    let movieIndex = tableModels[dayIndex!].cinemaDetailsSection.index(where: {$0.movie.id == movie.id})
                    if movieIndex != nil {
                        // Movie exist
                        tableModels[dayIndex!].cinemaDetailsSection[movieIndex!].timing.append(newTiming)
                    } else {
                        // Movie doesn't exists
                        let newMovie = Movie2(id: movie.id, title: movie.title, synoposis: movie.synoposis, rating: movie.rating, genre: movie.genre, runtime: movie.runtime, imgPosterUrl: movie.imgPosterUrl, imgBannerUrl: movie.imgBannerUrl, trailerUrl: movie.trailerUrl, is3D: movie.is3D, timingList: nil)
                        var newCinemaDetailsSection = CinemaDetailsSectionModel(movie: newMovie)
                        newCinemaDetailsSection.timing.append(newTiming)
                        tableModels[dayIndex!].cinemaDetailsSection.append(newCinemaDetailsSection)
                    }
                } else {
                    // day model doesn't exist
                    var newCinemaDetailsTableModel = CinemaDetailsTableModel(date: timing.date!)
                    let newMovie = Movie2(id: movie.id, title: movie.title, synoposis: movie.synoposis, rating: movie.rating, genre: movie.genre, runtime: movie.runtime, imgPosterUrl: movie.imgPosterUrl, imgBannerUrl: movie.imgBannerUrl, trailerUrl: movie.trailerUrl, is3D: movie.is3D, timingList: nil)
                    var newCinemaDetailsSection = CinemaDetailsSectionModel(movie: newMovie)
                    newCinemaDetailsSection.timing.append(newTiming)
                    newCinemaDetailsTableModel.cinemaDetailsSection.append(newCinemaDetailsSection)
                    tableModels.append(newCinemaDetailsTableModel)
                }
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mma"
        for dayIndex in 0..<tableModels.count {
            tableModels[dayIndex].cinemaDetailsSection.sort(by: {$0.movie.title! < $1.movie.title!})
            for sectionIndex in 0..<tableModels[dayIndex].cinemaDetailsSection.count {
                tableModels[dayIndex].cinemaDetailsSection[sectionIndex].timing.sort { (timing1, timing2) -> Bool in
                    var date1, date2: Date
                    if (timing1.time!.hasSuffix("AM") || timing1.time!.hasSuffix("PM")) &&
                        (timing2.time!.hasSuffix("AM") || timing2.time!.hasSuffix("PM")) {
                        date1 = dateFormatter.date(from: timing1.time!)!
                        date2 = dateFormatter.date(from: timing2.time!)!
                        return date1 < date2
                    }
                    return false
                }
                let earliestTime = tableModels[dayIndex].cinemaDetailsSection[sectionIndex].timing[0].time!
                if earliestTime.hasSuffix("AM") || earliestTime.hasSuffix("PM"){
                    tableModels[dayIndex].cinemaDetailsSection[sectionIndex].earliestTimeSlot = dateFormatter.date(from: earliestTime)!
                }
            }
        }
        
        for i in 0..<self.availableDates.count {
            if let index = self.tableModels.index(where: {$0.date == self.availableDates[i]}) {
                if self.tableModels[index].cinemaDetailsSection.count != 0 {
                    self.daySemgmentControl.selectedSegmentIndex = i
                    self.tableModelIndex = index
                    break
                }
            }
        }
        if self.tableModelIndex == nil {
            self.tableModelIndex = self.tableModels.index(where: {$0.date == self.availableDates[0]})
        }
        
        return tableModels
    }
    
    func sortByTitle() {
        for dayIndex in 0..<tableModels.count {
            tableModels[dayIndex].cinemaDetailsSection.sort(by: {$0.movie.title! < $1.movie.title!})
        }
    }
    
    func sortByEarliestTime() {
        for dayIndex in 0..<tableModels.count {
            tableModels[dayIndex].cinemaDetailsSection.sort(by: {$0.earliestTimeSlot!.compare($1.earliestTimeSlot!) == .orderedAscending})
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetailsFromCinema" {
            if tableModelIndex != nil {
                if let indexPath = cinemaTable.indexPathForSelectedRow {
                    cinemaTable.deselectRow(at: indexPath, animated: true)
                    let controller = segue.destination as! MovieDetailsVC
                    controller.detailMovie =                    tableModels[tableModelIndex!].cinemaDetailsSection[indexPath.section].movie
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CinemaDetailsVC: CustomCollectionCellDelegate {
    func collectionView(collectioncell: TimeCell?, didTappedInTableView TableCell: CinemaCell) {
        if let cell = collectioncell {
            if cell.link != "" {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "BookTimingVC") as? BookTimingVC
                controller?.link = cell.link
                self.navigationController?.pushViewController(controller!, animated: true)
            } else {
                let alert = UIAlertController(title: "Sold out!", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }
    }
    
    
}
