//
//  SearchViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 16/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SearchVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    var dataPicker: UIPickerView!
    var toolBar: UIToolbar?
    
    var searchData: SearchData2?
    var searchDataMovies: [Movie2]?
    var searchDataCinemas: [Cinema2]?
    var searchDataDays: [String]?
    var searchDataDates: [Date]?
    
    var selectedDate: Date?
    var selectedDay: String?
    var selectedMovie: Movie2?
    var selectedCinema: Cinema2?
    
    var searchDetailsData: SearchDetailsData2?

    @IBOutlet weak var dayTextField: UITextField!
    @IBOutlet weak var movieTextField: UITextField!
    @IBOutlet weak var cinemaTextField: UITextField!
    
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createDayAndDates()
        fetchSearchData()
        setupDataPickerAndToolBar()
        setupTextFields()
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func searchButtonAction(_ sender: UIButton) {
        if selectedDate != nil && selectedCinema != nil && selectedMovie != nil {
            fetchSearchDetails(selectedDate: selectedDate!, selectedMovie: selectedMovie!, selectedCinema: selectedCinema!)
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            if let searchDataDays = searchDataDays {
                return searchDataDays.count
            }
        } else if pickerView.tag == 1 {
            if let searchDataMovies = searchDataMovies {
                return searchDataMovies.count
            }
        } else if pickerView.tag == 2 {
            if let searchDataCinemas = searchDataCinemas {
                return searchDataCinemas.count
            }
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame: pickerView.frame)
        label.textAlignment = .center
        label.font = UIFont(name: "SFUIDisplay", size: 21)
        label.adjustsFontSizeToFitWidth = true
        if pickerView.tag == 0 {
            if let searchDataDays = searchDataDays {
                label.text = searchDataDays[row]
            }
        } else if pickerView.tag == 1 {
            if let searchDataMovies = searchDataMovies {
                label.text = getMovieTitle(movie: searchDataMovies[row])
            }
        } else if pickerView.tag == 2 {
            if let searchDataCinemas = searchDataCinemas {
                label.text = getCinemaTitle(cinema: searchDataCinemas[row])
            }
        }
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            if let searchDataDates = searchDataDates {
                selectedDate = searchDataDates[row]
                dayTextField.text = searchDataDays![row]
            }
        } else if pickerView.tag == 1 {
            if let searchDataMovies = searchDataMovies {
                selectedMovie = searchDataMovies[row]
                movieTextField.text = getMovieTitle(movie: selectedMovie!)
            }
        } else if pickerView.tag == 2 {
            if let searchDataCinemas = searchDataCinemas {
                selectedCinema = searchDataCinemas[row]
                cinemaTextField.text = getCinemaTitle(cinema: selectedCinema!)
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dayTextField {
            dataPicker.tag = 0
        } else if textField == movieTextField {
            dataPicker.tag = 1
        } else if textField == cinemaTextField {
            dataPicker.tag = 2
        }
        dataPicker.reloadAllComponents()
        print(dataPicker.tag)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    func fetchSearchData() {
        let url = URL(string: "http://52.230.25.86:4567/search")
        //let url = URL(string: "http://192.168.1.91:4567/search")
        print("URL: \(url!)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    self.searchData = try JSONDecoder().decode(SearchResponse.self, from: data!).data
                    self.searchDataMovies = self.searchData!.movieList.sorted(by: {$0.title! < $1.title!})
                    self.searchDataCinemas = self.searchData!.cinemaList.sorted(by: { (cinema1, cinema2) -> Bool in
                        if cinema1.company! < cinema2.company! {
                            return true
                        }
                        if cinema1.company! > cinema2.company! {
                            return false
                        }
                        if cinema1.name! < cinema2.name! {
                            return true
                        } else {
                            return false
                        }
                        
                    })
                } catch {
                    print("Parse Error: \(error)")
                }
                DispatchQueue.main.async {
                    print("Done with url")
                    self.dataPicker.reloadAllComponents()
                }
            }
            }.resume()
    }
    
    func fetchSearchDetails(selectedDate: Date, selectedMovie: Movie2, selectedCinema: Cinema2) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyy-MM-dd"
        let selectedDay = formatter.string(from: selectedDate)
        let fullUrl = "http://52.230.25.86:4567/searchDetails?date=" + selectedDay + "&idmovie=\(selectedMovie.id)&idcinema=\(selectedCinema.id)"
        //var fullUrl = "http://192.168.1.91:4567/searchDetails?date=" + selectedDay + "&idmovie=\(selectedMovie.id)&idcinema=\(selectedCinema.idcinema)"
        let url = URL(string: fullUrl)
        var exist: Bool?
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    print("wtff wh")
                    self.searchDetailsData = try JSONDecoder().decode(SearchDetailsResponse.self, from: data!).data
                    exist = self.searchDetailsData?.exist
                    
                } catch {
                    let output = String(data: data!, encoding: String.Encoding.utf8) as String?
                    print("Parse Error: \(error)\nRaw Data: \(output!)")
                }
                DispatchQueue.main.async {
                    print("Done with url")
                    if let exist = exist {
                        if exist {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "SearchDetailsVC") as? SearchDetailsVC
                            controller?.searchDetailsData = self.searchDetailsData!
                            self.navigationController?.pushViewController(controller!, animated: true)
                        } else {
                            let alert = UIAlertController(title: "No timings available", message: nil, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
            }.resume()
    }
    
    func fetchSearchExist(selectedDate: Date, selectedMovie: MovieSearch?, selectedCinema: CinemaSearch?) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyy-MM-dd"
        let selectedDay = formatter.string(from: selectedDate)
        var fullUrl = "http://52.230.25.86:4567/searchExist?date="+selectedDay
        //var fullUrl = "http://192.168.1.91:4567/searchExist?date="+selectedDay
        var type: Int?
        if selectedMovie != nil && selectedCinema != nil {
            fullUrl = fullUrl + "&idmovie=\(selectedMovie!.id)&idcinema=\(selectedCinema!.idcinema)"
            type = 0
        } else if selectedMovie != nil {
            fullUrl = fullUrl + "&idmovie=\(selectedMovie!.id)&idcinema=0"
            type = 1
        } else if selectedCinema != nil {
            fullUrl = fullUrl + "&idmovie=0&idcinema=\(selectedCinema!.idcinema)"
            type = 2
        }
        let url = URL(string: fullUrl)
        var exist: Bool?
        print("URL: \(url!)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    print("wtff wh")
                    exist = try JSONDecoder().decode(SearchExist.self, from: data!).data.exist
                    print("Exist: \(exist!)")
                    
                } catch {
                    let output = String(data: data!, encoding: String.Encoding.utf8) as String!
                    print("Parse Error: \(error)\nRaw Data: \(output!)")
                }
                DispatchQueue.main.async {
                    print("Done with url")
                    if let exist = exist {
                        if let type = type {
                            print("Type: \(type)")
                            if type == 0 {
                                if exist {
                                    
                                    
//                                    self.performSegue(withIdentifier: "showSearchDetails", sender: self)
                                    print("PUSHPUSH")
                                } else {
                                    // no showtimes for that combination
                                }
                            } else if type == 1 {
                                if exist {
                                    // selected movie is showing on the selected day, jump to timingsVC
//                                    let controller = storyboard.instantiateViewController(withIdentifier: "TimingsVC") as! TimingsVC
//                                    controller.movieId = selectedMovie!.id
//                                    self.navigationController?.pushViewController(controller, animated: true)
                                } else {
                                    // selected movie is not showing on the selected day
                                }
                            } else if type == 2 {
                                if exist {
                                    // selected cinema is showing movies on selected day
                                    
                                }
                            }
                        }
                    }
                }
            }
            }.resume()
    }
    
    func createDayAndDates() {
        searchDataDates = [Date]()
        searchDataDays = ["Today","Tomorrow"]
        var today = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let dayFormatter = DateFormatter()
        dayFormatter.dateFormat = "EEEE"
        for i in 0...2 {
            let result = formatter.string(from: today)
            searchDataDates?.append(today)
            today = Calendar.current.date(byAdding: .day, value: 1, to: today)!
        }
        today = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
        searchDataDays?.append(dayFormatter.string(from: today))
        selectedDate = searchDataDates![0]
    }
    
    @objc func donePicker(sender: UIBarButtonItem) {
        hidePicker(tag: dataPicker.tag)
    }
    
    @objc func nextTextField(sender: UIBarButtonItem) {
        
    }
    
    @objc func previousTextField(sender: UIBarButtonItem) {
        
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        print("identifier: \(segue.identifier)")
//        if segue.identifier == "showSearchDetails" {
//            print("identified")
//            let controller = segue.destination as! SearchDetailsViewController
//            controller.searchDetailsData = searchDetailsData
//        }
//    }
    
    func hidePicker(tag: Int) {
        if tag == 0 {
            dayTextField.resignFirstResponder()
        } else if tag == 1 {
            movieTextField.resignFirstResponder()
        } else if tag == 2 {
            cinemaTextField.resignFirstResponder()
        }
    }
    
    func getMovieTitle(movie: Movie2) -> String {
        if movie.is3D {
            return movie.title! + " (3D)"
        }
        return movie.title!
    }
    
    func getCinemaTitle(cinema: Cinema2) -> String {
        if cinema.customType! != "Normal" {
            return cinema.name! + " (" + cinema.customType! + ")"
        }
        return cinema.name!
    }
    
    func setupDataPickerAndToolBar() {
        dataPicker = UIPickerView()
        dataPicker.delegate = self
        dataPicker.dataSource = self
        toolBar = UIToolbar()
        toolBar?.barStyle = UIBarStyle.default
        toolBar?.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let flexibleSpaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let fixedSpaceButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        let nextButton = UIBarButtonItem(image: UIImage(named: "Backward Arrow"), style: .plain, target: self, action: #selector(nextTextField))
        nextButton.width = 30
        let previousButton = UIBarButtonItem(image: UIImage(named: "Forward Arrow"), style: .plain, target: self, action: #selector(previousTextField))
        previousButton.width = 30
        toolBar?.setItems([fixedSpaceButton, nextButton, fixedSpaceButton, previousButton, flexibleSpaceButton, doneButton], animated: false)
        toolBar?.isUserInteractionEnabled = true
    }
    
    func setupTextFields() {
        dayTextField.delegate = self
        movieTextField.delegate = self
        cinemaTextField.delegate = self
        dayTextField.inputView = dataPicker
        movieTextField.inputView = dataPicker
        cinemaTextField.inputView = dataPicker
        dayTextField.inputAccessoryView = toolBar
        movieTextField.inputAccessoryView = toolBar
        cinemaTextField.inputAccessoryView = toolBar
    }
}
