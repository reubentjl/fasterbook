//
//  MoviesViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 5/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MoviesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var movies = [Movie2]()
    var filteredMovies = [Movie2]()
    
    @IBOutlet weak var adBanner: GADBannerView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredMovies.count
        }
        return movies.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie : Movie2
        if isFiltering() {
            movie = filteredMovies[indexPath.row]
        } else {
            movie = movies[indexPath.row]
        }
        let cell = moviesTable.dequeueReusableCell(withIdentifier: "MoviesCell") as! MoviesCell
        cell.posterImage.contentMode = .scaleAspectFill
        if movie.imgPosterUrl!.starts(with: "http") {
            cell.posterImage.downloadedFrom(link: movie.imgPosterUrl!)
        } else {
            cell.posterImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w185/" + movie.imgPosterUrl!)
        }
        
        if movie.is3D {
            cell.is3DLabel.text = "3D"
            cell.is3DLabel.textColor = .white
            cell.is3DLabel.layer.shadowColor = UIColor.black.cgColor
            cell.is3DLabel.layer.shadowRadius = 3.0
            cell.is3DLabel.layer.shadowOpacity = 1.0
            cell.is3DLabel.shadowOffset = CGSize(width: 3, height: 3)
            cell.is3DLabel.layer.masksToBounds = false
        } else {
            cell.is3DLabel.text = ""
        }
        var finalTitle = movie.title!
        if let rating = movie.rating {
            if rating != "-" {
                finalTitle = finalTitle + " (" + rating + ")"
            }
        }
        cell.titleLabel.text = finalTitle
        if movie.runtime != 0 {
            cell.runtimeLabel.text = "\(movie.runtime!) mins"
        } else {
            cell.runtimeLabel.text = ""
        }
        if let genre = movie.genre {
            if genre == "null" {
                cell.genreLabel.text = ""
            } else {
                cell.genreLabel.text = movie.genre
            }
        } else {
            cell.genreLabel.text = ""
        }
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }

    @IBOutlet weak var moviesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search titles"
        navigationItem.title = "Movies"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        moviesTable.delegate = self
        moviesTable.dataSource = self
        
        let url = URL(string: "http://52.230.25.86:4567/featured")
        //let url = URL(string: "http://192.168.1.91:4567/featured")
        var responseData: MoviesResponse!
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    print("Data:")
                    print(data!)
                    if let responseData = try JSONDecoder().decode(MoviesResponse.self, from: data!).data {
                        self.movies = responseData
                    }
                    self.movies.sort(by: {$0.title! < $1.title!})
                } catch {
                    print("Parse Error: \(error)")
                    let alert = UIAlertController(title: "Error parsing Data", message: error as! String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                
                DispatchQueue.main.async {
                    self.moviesTable.reloadData()
                }
            } else {
                let alert = UIAlertController(title: "Error Loading Data", message: error.debugDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            }.resume()
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func fiilterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredMovies = movies.filter( { (movie : Movie2) -> Bool in
            return movie.title!.lowercased().contains(searchText.lowercased())
            } )
        moviesTable.reloadData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetails" {
            if let indexPath = moviesTable.indexPathForSelectedRow {
                moviesTable.deselectRow(at: indexPath, animated: true)
                let movie : Movie2
                if isFiltering() {
                    movie = filteredMovies[indexPath.row]
                } else {
                    movie = movies[indexPath.row]
                }
                let controller = segue.destination as! MovieDetailsVC
                controller.detailMovie = movie
            }
        }
    }
}

extension MoviesVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        fiilterContentForSearchText(searchController.searchBar.text!)
    }
}

extension MoviesVC: ShowTimingButtonDelegate {
    func showTimingButtonTapped(at index: IndexPath) {
        print("received")
        let movie : Movie2
        if isFiltering() {
            movie = filteredMovies[index.row]
        } else {
            movie = movies[index.row]
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "TimingsVC") as? TimingsVC
        controller?.movieId = movie.id
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}
