//
//  CinemasVC.swift
//  FasterBook
//
//  Created by Reuben Tan on 6/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class CinemasVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var cinemaTable: UITableView!
    var cinemas = [Cinema]()
    var cinemaTableModel = [CinemasMoviesSection]()
    var filteredCinemaTableModel = [CinemasMoviesSection]()
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var adBanner: GADBannerView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *){
            navigationItem.hidesSearchBarWhenScrolling = false
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = true
        }
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Cinemas"
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search cinemas"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        cinemaTable.delegate = self
        cinemaTable.dataSource = self
        getCinemasMoviesData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredCinemaTableModel[section].rows.count
        }
        return cinemaTableModel[section].rows.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering() {
            return filteredCinemaTableModel.count
        }
        return cinemaTableModel.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering() {
            return filteredCinemaTableModel[section].section
        }
        return cinemaTableModel[section].section
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cinema : CinemasMoviesRow
        if isFiltering() {
            cinema = filteredCinemaTableModel[indexPath.section].rows[indexPath.row]
        } else {
            cinema = cinemaTableModel[indexPath.section].rows[indexPath.row]
        }
        if cinema.collections.count != 0 {
            let cell = cinemaTable.dequeueReusableCell(withIdentifier: "CinemasMovieCell", for: indexPath) as! CinemasMovieCell
            if cinema.customType != "Normal" {
                cell.nameLabel.text = cinema.cinemaName + " (" + cinema.customType + ")"
            } else {
                cell.nameLabel.text = cinema.cinemaName
            }
            
            cell.movies = cinema.collections
            cell.cellDelegate = self
            
            cell.indexPath = indexPath
            cell.imagesCollection.reloadData()
            return cell
        } else {
            let cell = cinemaTable.dequeueReusableCell(withIdentifier: "CinemasCell") as! CinemasCell
            if cinema.customType != "Normal" {
                cell.nameLabel.text = cinema.cinemaName + " (" + cinema.customType + ")"
            } else {
                cell.nameLabel.text = cinema.cinemaName
            }
            cell.addressLabel.text = "No movies available today"
            return cell
        }
        
//        let cell = cinemaTable.dequeueReusableCell(withIdentifier: "CinemasCell") as! CinemasCell
//        cell.nameLabel.text = cinema.cinemaName
//        cell.addressLabel.text = cinema.address.replacingOccurrences(of: "\\n", with: "\n")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cinema : CinemasMoviesRow
        if isFiltering() {
            cinema = filteredCinemaTableModel[indexPath.section].rows[indexPath.row]
        } else {
            cinema = cinemaTableModel[indexPath.section].rows[indexPath.row]
        }
        if cinema.collections.count != 0 {
            return 188
        }
        return UITableViewAutomaticDimension
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func filterContentForSearchtext(_ searchText: String, scope: String = "ALL") {
        filteredCinemaTableModel = [CinemasMoviesSection]()
        for section in cinemaTableModel {
            for cinema in section.rows {
                if (cinema.cinemaName.lowercased().contains(searchText.lowercased())) {
                    if let index = filteredCinemaTableModel.index(where: {$0.section == section.section}) {
                        filteredCinemaTableModel[index].rows.append(cinema)
                    } else {
                        var newCinemaSection = CinemasMoviesSection(section: section.section)
                        newCinemaSection.rows.append(cinema)
                        filteredCinemaTableModel.append(newCinemaSection)
                    }
                }
            }
        }
//        filteredCinemaTableModel = cinemaTableModel.filter({$0.cinemas.contains(where:{
//            $0.cinemaName.lowercased().contains(searchText.lowercased())
//        })})
        cinemaTable.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCinemaDetails" {
            if let indexPath = cinemaTable.indexPathForSelectedRow {
                cinemaTable.deselectRow(at: indexPath, animated: true)
                let controller = segue.destination as! CinemaDetailsVC
                let idcinema: Int
                var cinemaName: String
                if isFiltering() {
                    idcinema = filteredCinemaTableModel[indexPath.section].rows[indexPath.row].idCinema
                    cinemaName = filteredCinemaTableModel[indexPath.section].rows[indexPath.row].cinemaName
                    if filteredCinemaTableModel[indexPath.section].rows[indexPath.row].customType != "Normal" {
                        cinemaName = cinemaName + " (" + filteredCinemaTableModel[indexPath.section].rows[indexPath.row].customType + ")"
                    }
                } else {
                    idcinema = cinemaTableModel[indexPath.section].rows[indexPath.row].idCinema
                    cinemaName = cinemaTableModel[indexPath.section].rows[indexPath.row].cinemaName
                    if cinemaTableModel[indexPath.section].rows[indexPath.row].customType != "Normal" {
                        cinemaName = cinemaName + " (" + cinemaTableModel[indexPath.section].rows[indexPath.row].customType + ")"
                    }
                }
                controller.idcinema = idcinema
                controller.cinemaName = cinemaName
                controller.navigationItem.title = controller.cinemaName
            }
        }
    }
    
    func getCinemasMoviesData() {
        let url = URL(string: "http://52.230.25.86:4567/cinemasMovies")
        //let url = URL(string: "http://192.168.1.91:4567/cinemasMovies")
        var cinemas = [Cinema2]()
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    print("URL: \(url!)")
                    print("Data:")
                    print(data!)
                    cinemas = try JSONDecoder().decode(CinemasMovies2.self, from: data!).data
                } catch {
                    print("Parse Error: \(error)")
                    let alert = UIAlertController(title: "Error Parsing Data", message: error as! String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                
                DispatchQueue.main.async {
                    for cinema in cinemas {
                        var modelIndexNo: Int
                        var cinemaIndexNo: Int
                        if let modelIndex = self.cinemaTableModel.index(where: {$0.section == cinema.company}) {
                            // Company exists
                            modelIndexNo = modelIndex
                            if let cinemaIndex = self.cinemaTableModel[modelIndex].rows.index(where: {$0.idCinema == cinema.id}) {
                                // cinema exists
                                cinemaIndexNo = cinemaIndex
                            } else {
                                // cinema doesn't exists
                                var newCinema = CinemasMoviesRow(id: cinema.id, name: cinema.name!, neighbourhood: cinema.neighbourhood!, customType: cinema.customType!)
                                self.cinemaTableModel[modelIndex].rows.append(newCinema)
                                cinemaIndexNo = self.cinemaTableModel[modelIndex].rows.index(where: {$0.idCinema == cinema.id})!
                            }
                        } else {
                            // Company doesnt exist
                            var newCinema = CinemasMoviesRow(id: cinema.id, name: cinema.name!, neighbourhood: cinema.neighbourhood!, customType: cinema.customType!)
                            var newCompany = CinemasMoviesSection(section: cinema.company!)
                            newCompany.rows.append(newCinema)
                            cinemaIndexNo = newCompany.rows.index(where: {$0.idCinema == cinema.id})!
                            self.cinemaTableModel.append(newCompany)
                            modelIndexNo = self.cinemaTableModel.index(where: {$0.section == cinema.company})!
                        }
                        for movie in cinema.showingMovieList! {
                            if movie.imgPosterUrl != nil {
                                let newMovie = CinemaMoviesCollection(idmovie: movie.id, posterPath: movie.imgPosterUrl!, is3D: movie.is3D)
                                self.cinemaTableModel[modelIndexNo].rows[cinemaIndexNo].collections.append(newMovie)
                            }
                        }
                    }
                    print(self.cinemaTableModel.count)
                    self.cinemaTableModel.sort(by: {$0.section < $1.section})
                    for i in 0..<self.cinemaTableModel.count {
                        self.cinemaTableModel[i].rows.sort(by: {$0.cinemaName < $1.cinemaName})
                        for j in 0..<self.cinemaTableModel[i].rows.count {
                            self.cinemaTableModel[i].rows[j].collections.sort(by: {$0.idmovie > $1.idmovie})
                        }
                    }
                    self.cinemaTable.reloadData()
                }
            } else {
                let alert = UIAlertController(title: "Error Loading Data", message: error.debugDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            }.resume()
    }
    
//    func getData() {
//        let url = URL(string: "http://192.168.1.91:4567/cinemas")
//        var responseData: Cinemas!
//        URLSession.shared.dataTask(with: url!) { (data, response, error) in
//            if error == nil {
//                do {
//                    print("URL: \(url)")
//                    print("Data:")
//                    print(data!)
//                    responseData = try JSONDecoder().decode(Cinemas.self, from: data!)
//                    self.cinemas = responseData.data
//                } catch {
//                    print("Parse Error: \(error)")
//                }
//
//                DispatchQueue.main.async {
//                    for cinema in self.cinemas {
//                        let modelIndex = self.cinemaTableModel.index(where: {$0.company == cinema.cinemaCompany.rawValue})
//                        if modelIndex != nil {
//                            self.cinemaTableModel[modelIndex!].cinemas.append(cinema)
//                        } else {
//                            var newCinemaModel = CinemaTableModel(company: cinema.cinemaCompany.rawValue)
//                            newCinemaModel.cinemas.append(cinema)
//                            self.cinemaTableModel.append(newCinemaModel)
//                        }
//                    }
//                    self.cinemaTableModel.sort(by: {$0.company < $1.company})
//                    for i in 0..<self.cinemaTableModel.count {
//                        self.cinemaTableModel[i].cinemas.sort(by: {$0.cinemaName < $1.cinemaName})
//                    }
//                    self.cinemaTable.reloadData()
//                }
//            }
//            }.resume()
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CinemasVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchtext(searchController.searchBar.text!)
    }
}

extension CinemasVC: CinemasMovieCellDelegate {
    func collectionViewImgSelected(collectioncell: CinemasImageCell?, didTappedInTableView tableCell: CinemasMovieCell) {
        if let cell = collectioncell {
            let indexPath = tableCell.indexPath!
            cinemaTable.deselectRow(at: indexPath, animated: true)
            let idcinema: Int!
            var cinemaName: String
            if isFiltering() {
                idcinema = filteredCinemaTableModel[indexPath.section].rows[indexPath.row].idCinema
                cinemaName = filteredCinemaTableModel[indexPath.section].rows[indexPath.row].cinemaName
                if filteredCinemaTableModel[indexPath.section].rows[indexPath.row].customType != "Normal" {
                    cinemaName = cinemaName + " (" + filteredCinemaTableModel[indexPath.section].rows[indexPath.row].customType + ")"
                }
            } else {
                idcinema = cinemaTableModel[indexPath.section].rows[indexPath.row].idCinema
                cinemaName = cinemaTableModel[indexPath.section].rows[indexPath.row].cinemaName
                if cinemaTableModel[indexPath.section].rows[indexPath.row].customType != "Normal" {
                    cinemaName = cinemaName + " (" + cinemaTableModel[indexPath.section].rows[indexPath.row].customType + ")"
                }
            }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "CinemaDetailsVC") as? CinemaDetailsVC
            controller?.idcinema = idcinema
            controller?.cinemaName = cinemaName
            controller?.scrollToMovieId = cell.movieId
            controller?.navigationItem.title = controller?.cinemaName
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}
