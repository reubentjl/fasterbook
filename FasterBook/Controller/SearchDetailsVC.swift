//
//  SearchDetailsViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 11/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SearchDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var searchDetailsTable: UITableView!
    var searchDetailsData: SearchDetailsData2?
    var movie: Movie2?
    var cinema: Cinema2?
    
    @IBOutlet weak var adBanner: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchDetailsTable.delegate = self
        searchDetailsTable.dataSource = self
        if searchDetailsData != nil {
            movie = searchDetailsData!.movie
            if movie!.is3D {
                navigationItem.title = movie!.title! + " (3D)"
            } else {
                navigationItem.title = movie!.title
            }
            cinema = searchDetailsData!.cinema
        }
        
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2519986665224487~7941311264")
        adBanner.adSize = kGADAdSizeBanner
        adBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        adBanner.rootViewController = self
        adBanner.load(GADRequest())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var noOfSection = 0
        if searchDetailsData != nil {
            noOfSection = 2
        }
        if let movie = movie {
            if movie.synoposis != "" {
                noOfSection += 1
            }
            if movie.genre != "" {
                noOfSection += 1
            }
            if movie.runtime != 0 {
                noOfSection += 1
            }
            if movie.rating != "" {
                noOfSection += 1
            }
            if movie.trailerUrl != "" {
                noOfSection += 1
            }
            if movie.imgBannerUrl != "" && movie.imgPosterUrl != "" {
                noOfSection += 1
            }
        }
        return 8
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let movie = movie, let cinema = cinema {
            if section == SearchDetailsTableEnum.cinemaInfo.rawValue {
                return (cinema.name != nil) ? "Cinema Info" : ""
            } else if section == SearchDetailsTableEnum.timings.rawValue {
                return (cinema.timingList?.count != 0) ? "Timings" : ""
            } else if section == SearchDetailsTableEnum.trailer.rawValue {
                return (movie.trailerUrl != nil && movie.trailerUrl != "") ? "Trailer" : ""
            } else if section == SearchDetailsTableEnum.synopsis.rawValue {
                return (movie.synoposis != nil && movie.synoposis != "") ? "Synopsis" : ""
            } else if section == SearchDetailsTableEnum.genre.rawValue {
                return (movie.genre != nil && movie.genre != "") ? "Genre" : ""
            } else if section == SearchDetailsTableEnum.runtime.rawValue {
                return (movie.runtime != nil && movie.runtime != 0) ? "Runtime" : ""
            } else if section == SearchDetailsTableEnum.rating.rawValue {
                return (movie.rating != nil && movie.rating != "") ? "Rating" : ""
            } else if section == SearchDetailsTableEnum.images.rawValue {
                return ((movie.imgBannerUrl != nil || movie.imgPosterUrl != nil) && (movie.imgBannerUrl != "" || movie.imgPosterUrl != "")) ? "Images":""
            }
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let movie = movie, let cinema = cinema {
            if section == SearchDetailsTableEnum.cinemaInfo.rawValue {
                return (cinema.name != nil) ? 1 : 0
            } else if section == SearchDetailsTableEnum.timings.rawValue {
                return (cinema.timingList?.count != 0) ? 1 : 0
            } else if section == SearchDetailsTableEnum.trailer.rawValue {
                return (movie.trailerUrl != nil && movie.trailerUrl != "") ? 1 : 0
            } else if section == SearchDetailsTableEnum.synopsis.rawValue {
                return (movie.synoposis != nil && movie.synoposis != "") ? 1 : 0
            } else if section == SearchDetailsTableEnum.genre.rawValue {
                return (movie.genre != nil && movie.genre != "") ? 1 : 0
            } else if section == SearchDetailsTableEnum.runtime.rawValue {
                return (movie.runtime != nil && movie.runtime != 0) ? 1 : 0
            } else if section == SearchDetailsTableEnum.rating.rawValue {
                return (movie.rating != nil && movie.rating != "") ? 1 : 0
            } else if section == SearchDetailsTableEnum.images.rawValue {
                return ((movie.imgBannerUrl != nil || movie.imgPosterUrl != nil) && (movie.imgBannerUrl != "" || movie.imgPosterUrl != "")) ? 1 : 0
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let movie = movie, let cinema = cinema {
            let section = indexPath.section
            if section == SearchDetailsTableEnum.cinemaInfo.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "CinemaCell", for: indexPath) as! CinemasCell
                if cinema.customType != "Normal" {
                    cell.nameLabel.text = cinema.company! + " (\(cinema.customType!)"
                } else {
                    cell.nameLabel.text = cinema.name!
                }
                cell.addressLabel.text = cinema.address!.replacingOccurrences(of: "\\n", with: "\n")
                return cell
            } else if section == SearchDetailsTableEnum.timings.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "TimingCell", for: indexPath) as! CinemaCell
                cell.cinemaLabel.text = "Click to book"
                cell.timings = cinema.timingList
                cell.timeCollection.reloadData()
                cell.cellDelegate = self
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == SearchDetailsTableEnum.trailer.rawValue {
                if let trailerUrl = movie.trailerUrl {
                    let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "TrailerCell", for: indexPath) as! TrailerMovieDetailsCell
                    let url = URL(string: "https://www.youtube.com/embed/" + trailerUrl)
                    let request = URLRequest(url: url!)
                    cell.trailerWebView.load(request)
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    return cell
                }
            } else if section == SearchDetailsTableEnum.synopsis.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = movie.synoposis!
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == SearchDetailsTableEnum.genre.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = movie.genre!
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == SearchDetailsTableEnum.runtime.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = "\(movie.runtime!) mins"
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == SearchDetailsTableEnum.rating.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "SynopsisCell", for: indexPath) as! SynopsisMovieDetailsCell
                cell.synopsisTextView.text = movie.rating!
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            } else if section == SearchDetailsTableEnum.images.rawValue {
                let cell = searchDetailsTable.dequeueReusableCell(withIdentifier: "ImagesCell", for: indexPath) as! ImagesMovieDetailsCell
                if let imgPosterUrl = movie.imgPosterUrl {
                    cell.posterImage.contentMode = .scaleAspectFill
                    if imgPosterUrl.starts(with: "http") {
                        cell.posterImage.downloadedFrom(link: imgPosterUrl)
                    } else {
                        cell.posterImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w185/" + imgPosterUrl)
                    }
                }
                if let imgBannerUrl = movie.imgBannerUrl {
                    cell.bannerImage.contentMode = .scaleAspectFill
                    if imgBannerUrl.starts(with: "http") {
                        cell.bannerImage.downloadedFrom(link: imgBannerUrl)
                    } else {
                        cell.bannerImage.downloadedFrom(link: "https://image.tmdb.org/t/p/w500/" + imgBannerUrl)
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == SearchDetailsTableEnum.timings.rawValue {
            return 102
        }
        return UITableViewAutomaticDimension
    }
}

extension SearchDetailsVC: CustomCollectionCellDelegate {
    func collectionView(collectioncell: TimeCell?, didTappedInTableView TableCell: CinemaCell) {
        if let cell = collectioncell {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "BookTimingVC") as? BookTimingVC
            controller?.link = cell.link
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}






