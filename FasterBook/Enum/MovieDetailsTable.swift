//
//  MovieDetailsTable.swift
//  FasterBook
//
//  Created by Reuben Tan on 12/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import Foundation

enum MovieDetailsTableEnum: Int {
    case trailer = 0,
    synopsis, genre, runtime, rating, images
}

enum SearchDetailsTableEnum: Int {
    case cinemaInfo = 0,
    timings, trailer, synopsis, genre, runtime, rating, images
}
