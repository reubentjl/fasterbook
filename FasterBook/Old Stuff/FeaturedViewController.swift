//
//  FeaturedViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 16/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FeaturedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    
    @IBOutlet weak var AdBanner: GADBannerView!
    
    let titles = ["Deadpool 2", "Avengers: Infinity Wars", "Bad Samaritan", "Blockers", "Rampage"]
    let images = ["deadpool 2", "avengers", "bad samaritan", "blockers", "rampage"]
    let shortInfo = ["M18, 1h 59m","PG-13, 2h 29m","M18, 1h 51m","M18, 1h 42m","PG-13, 1h 47m"]
    let synopsis = [
        "After surviving a near fatal bovine attack, a disfigured cafeteria chef (Wade Wilson) struggles to fulfill his dream of becoming Mayberry's hottest bartender while also learning to cope with his lost sense of taste. Searching to regain his spice for life, as well as a flux capacitor, Wade must battle ninjas, the yakuza, and a pack of sexually aggressive canines, as he journeys around the world to discover the importance of family, friendship, and flavor - finding a new taste for adventure and earning the coveted coffee mug title of World's Best Lover.",
        "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
        "A pair of burglars stumble upon a woman being held captive in a home they intended to rob.",
        "When three parents discover their daughters’ pact to lose their virginity at prom, they launch a covert one-night operation to stop the teens from sealing the deal.",
        "Primatologist Davis Okoye shares an unshakable bond with George, the extraordinarily intelligent, silverback gorilla who has been in his care since birth. But a rogue genetic experiment gone awry mutates this gentle ape into a raging creature of enormous size. To make matters worse, it’s soon discovered there are other similarly altered animals. As these newly created alpha predators tear across North America, destroying everything in their path, Okoye teams with a discredited genetic engineer to secure an antidote, fighting his way through an ever-changing battlefield, not only to halt a global catastrophe but to save the fearsome creature that was once his friend."]
    var movies = [Movie2]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let url = URL(string: "http://52.230.25.86:4567/featured")
        //let url = URL(string: "http://192.168.1.91:4567/featured")
        var responseData: MoviesResponse!
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    print("Data:")
                    print(data!)
                    if let responseData = try JSONDecoder().decode(MoviesResponse.self, from: data!).data {
                        self.movies = responseData
                    }
                } catch {
                    print("Parse Error: \(error)")
                    let alert = UIAlertController(title: "Error Parsing Data", message: error as! String, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
                
                DispatchQueue.main.async {
                    self.featuredCollectionView.reloadData()
                }
            } else {
                let alert = UIAlertController(title: "Error Loading Data", message: error.debugDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
        }.resume()
        GADMobileAds.configure(withApplicationID: "2519986665224487~7941311264")
        AdBanner.adSize = kGADAdSizeSmartBannerPortrait
        AdBanner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        AdBanner.rootViewController = self
        AdBanner.load(GADRequest())
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return titles.count
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = featuredCollectionView.dequeueReusableCell(withReuseIdentifier: "featuredCell", for: indexPath) as! FeaturedCollectionViewCell
        cell.labelCell.text = movies[indexPath.row].title
        
        cell.imageCell.contentMode = .scaleAspectFit
        if movies[indexPath.row].imgPosterUrl!.starts(with: "http") {
            cell.imageCell.downloadedFrom(link: movies[indexPath.row].imgPosterUrl!)
        } else {
            cell.imageCell.downloadedFrom(link: "https://image.tmdb.org/t/p/w185/" + movies[indexPath.row].imgPosterUrl!)
        }
        
        
        //cell.labelCell.text = titles[indexPath.row]
        //cell.imageCell.image = UIImage(named: images[indexPath.row])
        
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 10 + 20
        let collectionViewSize = collectionView.frame.size.width - padding
        let height = collectionViewSize/2/128 * 192
        return CGSize(width: collectionViewSize/2, height: height + 17)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("hello")
        //self.performSegue(withIdentifier: "showMovieDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.featuredCollectionView.indexPath(for: cell) {
            let featuredDetailsVC = segue.destination as! FeaturedDetailsViewController
            let cover = Cover(imgBannerUrl: self.movies[indexPath.row].imgBannerUrl!,
                              rating: self.movies[indexPath.row].rating!,
                              runtime: self.movies[indexPath.row].runtime!)
            let synopsis = Synoposis(synoposis: self.movies[indexPath.row].synoposis!)
            let featuredSection = FeaturedSections(movie: self.movies[indexPath.row], coverSection: cover, synoposisSection: synopsis, timingsSection: nil)
            featuredDetailsVC.featuredSections = featuredSection
            featuredDetailsVC.title = self.movies[indexPath.row].title
//            let vc = segue.destination as! MovieDetailsViewController
//            vc.title = self.movies[indexPath.row].title
//            vc.movie = self.movies[indexPath.row]
        } 
    }
}
