//
//  FeaturedCoverTableViewCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 18/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class FeaturedCoverTableViewCell: UITableViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var shortTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib() 
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
