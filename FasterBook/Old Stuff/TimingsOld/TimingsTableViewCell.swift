//
//  TimingsTableViewCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 2/6/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class TimingsTableViewCell: UITableViewCell {
    @IBOutlet weak var timeCollectionView: UICollectionView!
    func setCollectionViewDataSourceDelegate <D: UICollectionViewDelegate & UICollectionViewDataSource> (_ dataSourceDelegate: D, forRow row: Int) {
        timeCollectionView.delegate = dataSourceDelegate
        timeCollectionView.dataSource = dataSourceDelegate
        timeCollectionView.tag = row
        timeCollectionView.reloadData()
    }
}
 
