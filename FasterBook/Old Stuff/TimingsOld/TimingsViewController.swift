//
//  TimingsViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 23/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class TimingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var timingsSegmentControl: UISegmentedControl!
    @IBOutlet weak var timingsTableView: UITableView!
    
    var id: Int?
    var showtime: ShowTimingResponse?
    var timingTableModels = [TimingTableModel]()
    var index: Int?
    var datesIndexMap: [String] = []
    
    func sortByCinema(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var timingTableModels = [TimingTableModel]()
        for i in 0..<showtime.data.count {
            datesIndexMap.append(showtime.data[i].date)
            let timingTableModel = TimingTableModel(date: showtime.data[i].date)
            var sectionModels = [TimingTableSectionModel]()
            for j in 0..<showtime.data[i].cinemaList.count {
                if let existsSectionModel = sectionModels.first(where: {$0.sectionTitle == showtime.data[i].cinemaList[j].company}) {
                    existsSectionModel.collectionModels.append(TimingTableCollectionModel(modelTitle: showtime.data[i].cinemaList[j].name!, timings: showtime.data[i].cinemaList[j].timingList!))
                } else {
                    let sectionModel = TimingTableSectionModel(
                        sectionTitle: showtime.data[i].cinemaList[j].company!)
                    let tableCollectionModel = TimingTableCollectionModel(
                        modelTitle: showtime.data[i].cinemaList[j].name!,
                        timings: showtime.data[i].cinemaList[j].timingList!)
                    sectionModel.collectionModels.append(tableCollectionModel)
                    sectionModels.append(sectionModel)
                }
            }
            for j in 0..<sectionModels.count {
                sectionModels[j].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
            sectionModels.sort(by: {$0.sectionTitle < $1.sectionTitle})
            timingTableModel.sectionModels = sectionModels
            timingTableModels.append(timingTableModel)
        }
        return timingTableModels
    }
    
    func sortByTiming(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var sortedTimingTableModels = [TimingTableModel]()
        
        for i in 0..<self.showtime!.data.count {
            let timingTableModel = TimingTableModel(date: (self.showtime?.data[i].date)!)
            var sectionModels = [TimingTableSectionModel]()
            for j in 0..<self.showtime!.data[i].cinemaList.count {
                let cinemaName = self.showtime!.data[i].cinemaList[j].name!
                for time in (self.showtime?.data[i].cinemaList[j].timingList!)! {
                    let hourAndAmOrPm : String
                    var date: Date
                    if time.time!.hasSuffix("AM") || time.time!.hasSuffix("PM") {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "h:mma"
                        date = dateFormatter.date(from: time.time!)!
                        dateFormatter.dateFormat = "h a"
                        hourAndAmOrPm = dateFormatter.string(from: date)
                        date = dateFormatter.date(from: hourAndAmOrPm)!
                    } else {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm"
                        date = dateFormatter.date(from: time.time!)!
                        dateFormatter.dateFormat = "h a"
                        hourAndAmOrPm = dateFormatter.string(from: date)
                        date = dateFormatter.date(from: hourAndAmOrPm)!
                    }
                    if let existsSectionModel = sectionModels.first(where: {$0.sectionTitle == hourAndAmOrPm}) {
                        if let cinemaCollectionModelExists = existsSectionModel.collectionModels.first(where: {$0.modelTitle == cinemaName}) {
                            cinemaCollectionModelExists.timings.append(time)
                        } else {
                            let timings = [time]
                            let collectionModel = TimingTableCollectionModel(modelTitle: cinemaName, timings: timings)
                            existsSectionModel.collectionModels.append(collectionModel)
                        }
                    } else {
                        let newSectionModel = TimingTableSectionModel(sectionTitle: hourAndAmOrPm)
                        newSectionModel.date = date
                        let timings = [time]
                        let collectionModel = TimingTableCollectionModel(modelTitle: cinemaName, timings: timings)
                        newSectionModel.collectionModels.append(collectionModel)
                        sectionModels.append(newSectionModel)
                    }
                }
            }
            sectionModels.sort(by: {$0.date!.compare($1.date!) == .orderedAscending})
            for j in 0..<sectionModels.count {
                sectionModels[j].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
            timingTableModel.sectionModels = sectionModels
            sortedTimingTableModels.append(timingTableModel)
        }
        
        return sortedTimingTableModels
    }
    
    func sortByNeighbourhood(showtime: ShowTimingResponse) -> Array<TimingTableModel> {
        var timingTableModels = [TimingTableModel]()
        for i in 0..<showtime.data.count {
            var timingTableModel = TimingTableModel(date: showtime.data[i].date)
            var sectionModels = [TimingTableSectionModel]()
            for j in 0..<showtime.data[i].cinemaList.count {
                if let existsSectionModel = sectionModels.first(where: {$0.sectionTitle == showtime.data[i].cinemaList[j].neighbourhood}) {
                    existsSectionModel.collectionModels.append(TimingTableCollectionModel(modelTitle: showtime.data[i].cinemaList[j].name!, timings: showtime.data[i].cinemaList[j].timingList!))
                } else {
                    var sectionModel = TimingTableSectionModel(
                        sectionTitle: showtime.data[i].cinemaList[j].neighbourhood!)
                    let tableCollectionModel = TimingTableCollectionModel(
                        modelTitle: showtime.data[i].cinemaList[j].name!,
                        timings: showtime.data[i].cinemaList[j].timingList!)
                    sectionModel.collectionModels.append(tableCollectionModel)
                    sectionModels.append(sectionModel)
                }
            }
            for j in 0..<sectionModels.count {
                sectionModels[j].collectionModels.sort(by: {$0.modelTitle < $1.modelTitle})
            }
            sectionModels.sort(by: {$0.sectionTitle < $1.sectionTitle})
            timingTableModel.sectionModels = sectionModels
            timingTableModels.append(timingTableModel)
        }
        return timingTableModels
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        timingsTableView.delegate = self
        timingsTableView.dataSource = self
        
        let url = URL(string: "http://52.230.25.86:4567/showtiming/\((self.id)!)")
        //let url = URL(string: "http://192.168.1.91:4567/showtiming/\((self.id)!)")
        print("URL: \(url!)")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    self.showtime = try JSONDecoder().decode(ShowTimingResponse.self, from: data!)
                } catch {
                    print("Parse Error: \(error)")
                }
                DispatchQueue.main.async {
                    self.timingTableModels = self.sortByCinema(showtime: self.showtime!)
                    var date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    for i in 0...2 {
                        let result = formatter.string(from: date)
                        if !self.timingTableModels.contains(where: {$0.date == result})  {
                            self.timingsSegmentControl.setEnabled(false, forSegmentAt: i)
                        }
                        date = Calendar.current.date(byAdding: .day, value: 1, to: date)!
                    }
                    date = Calendar.current.date(byAdding: .day, value: 2, to: Date())!
                    formatter.dateFormat = "EEEE"
                    self.timingsSegmentControl.setTitle(formatter.string(from: date), forSegmentAt: 2)
                    self.index = self.datesIndexMap.index(where: {$0 == self.timingTableModels[0].date})
                    self.timingsTableView.reloadData()
                }
            }
            }.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func timingsSegment(_ sender: Any) {
        index = (sender as AnyObject).selectedSegmentIndex
    }
    
    @IBAction func sortButton(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let a2zAction = UIAlertAction(title: "A-Z (Cinema)", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingTableModels = self.sortByCinema(showtime: self.showtime!)
        })
        let timingAction = UIAlertAction(title: "Timing", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingTableModels = self.sortByTiming(showtime: self.showtime!)
        })
        let neighbourhoodAction = UIAlertAction(title: "Neighbourhood", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            self.timingTableModels = self.sortByNeighbourhood(showtime: self.showtime!)
        })
        let nearestFirstAction = UIAlertAction(title: "Nearest First", style: .default, handler: {(alert: UIAlertAction!) -> Void in
            
        })
        alertController.addAction(a2zAction)
        alertController.addAction(timingAction)
        alertController.addAction(neighbourhoodAction)
        alertController.addAction(nearestFirstAction)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        }))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if index != nil {
            print("No of sections: \(timingTableModels[index!].sectionModels.count)")
            return timingTableModels[index!].sectionModels.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if index != nil {
            return timingTableModels[index!].sectionModels[section].collectionModels.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = timingsTableView.dequeueReusableCell(withIdentifier: "timeTCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? TimingsTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    } 
}

extension TimingsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let exists = index {
            let j = timingTableModels.index(where: {$0.date == datesIndexMap[exists]})
            if (j != -1) {
                return timingTableModels[j!].sectionModels[section].collectionModels[collectionView.tag].timings.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timeCCell", for: indexPath) as? TimingsCollectionViewCell
        cell?.timeLabel.text = timingTableModels[index!].sectionModels[indexPath.section].collectionModels[collectionView.tag].timings[indexPath.item].time
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let sectionheader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "timeCHeader", for: indexPath) as? TimingsCReUseView {
            sectionheader.cellHeaderLabel.text = timingTableModels[index!].sectionModels[indexPath.section].collectionModels[collectionView.tag].modelTitle
            return sectionheader
        }
        return UICollectionReusableView()
    }
}
