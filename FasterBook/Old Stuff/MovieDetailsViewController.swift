//
//  MovieDetailsViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 16/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var shortInfoTextView: UILabel!
    @IBOutlet weak var synopsisTextView: UITextView!
    
    var coverImageFileName: String?
    var shortInfoText: String?
    var synopsisText: String?
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coverImageView.contentMode = .scaleAspectFit
        coverImageView.downloadedFrom(link: "https://image.tmdb.org/t/p/w500/"+(movie?.imgBannerUrl)!)
        shortInfoTextView.text = "\((movie?.rating)!), \((movie?.runtime)!) mins"
        synopsisTextView.text = movie?.synoposis
        
        synopsisTextView.textContainerInset = UIEdgeInsets.zero
        synopsisTextView.textContainer.lineFragmentPadding = 0
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
