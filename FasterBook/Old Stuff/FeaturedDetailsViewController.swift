//
//  FeaturedDetailsViewController.swift
//  FasterBook
//
//  Created by Reuben Tan on 18/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class FeaturedDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var featuredDetailsTableView: UITableView!
    @IBOutlet weak var shortTextView: UILabel!
    var featuredSections: FeaturedSections?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.featuredDetailsTableView.dataSource = self
        self.featuredDetailsTableView.delegate = self
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.featuredSections?.noOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == featuredSectionIds.cover.rawValue {
            return self.featuredSections?.coverSection.noOfItems ?? 0
        } else if section == featuredSectionIds.synopsis.rawValue {
            return self.featuredSections?.synoposisSection.noOfItems ?? 0
        } else if section == featuredSectionIds.timing.rawValue {
            return self.featuredSections?.timingsSection?.timings.count ?? 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == featuredSectionIds.cover.rawValue {
            let cell = featuredDetailsTableView.dequeueReusableCell(withIdentifier: "coverCell") as! FeaturedCoverTableViewCell
            cell.shortTextLabel.text = "\((self.featuredSections?.coverSection.rating)!), \((self.featuredSections?.coverSection.runtime)!) mins"
            cell.coverImageView.contentMode = .scaleAspectFill
            cell.coverImageView.downloadedFrom(link: "https://image.tmdb.org/t/p/w500/"+(self.featuredSections?.coverSection.imgBannerUrl)!)
            return cell
        } else if indexPath.section == featuredSectionIds.synopsis.rawValue {
            let cell = featuredDetailsTableView.dequeueReusableCell(withIdentifier: "synopsisCell") as! FeaturedSynopsisCell
            cell.synopsisTextView.text = "\((self.featuredSections?.synoposisSection.synoposis)!)"
            return cell
        } else {
            let cell = featuredDetailsTableView.dequeueReusableCell(withIdentifier: "synopsisCell") as! FeaturedSynopsisCell
            cell.synopsisTextView.text = ""
            return cell
        }
    } 
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc1 = segue.destination as? TimingsViewController {
            vc1.id = (self.featuredSections?.movie.id)!
        }
    }
}
