//
//  FeaturedCollectionViewCell.swift
//  FasterBook
//
//  Created by Reuben Tan on 16/5/18.
//  Copyright © 2018 reubentjl. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelCell: UILabel!
}
